import java.io.*;
import java.net.*;
import java.util.*;
/**
 *  MailClient is a client program for the the Mail Protocol Mail System.
 *  It uses a command line interface to interact with the user.
 */
public class MailClient {
	static String HOST = "localhost";
	static int SERVER_PORT = 65535;
	static int TIMEOUT = 30000;
	static boolean quit = false;
	static BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
	static Socket sock;
	static BufferedReader in;
	static PrintWriter out;
	static String username;
	static final int REGISTER = 1;
	static final int MESSAGE = 2;
	static final int READ = 3;
	static final int QUIT = 4;
	
	public static void main(String[] args) throws Exception {
		if (args.length > 0) HOST = args[0];
		if (args.length > 1) SERVER_PORT = Integer.parseInt(args[1]);
		sock = new Socket(HOST, SERVER_PORT);
		sock.setSoTimeout(TIMEOUT);
		printWelcome();
	    out = new PrintWriter(sock.getOutputStream());
	    in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		while ( !quit ) {
			switch ( promptForAction() ) {
			case REGISTER: register(); break;
			case MESSAGE: sendMailMessage(); break;
			case READ: readInbox(); break;
			case QUIT: quit = true; break;
			}
		}
		socketSendMessage("END\n\n");
	}
	
	public static void socketSendMessage(String encoded) throws Exception {
		out.print(encoded);
		out.flush();
	}
	public static String socketReadMessage() throws Exception {

    	StringBuilder sb = new StringBuilder();
    	int rcv = 0;
    	char c = (char) 0, d = '\n';
    	boolean done = false;
    	while ( !done && rcv != -1) {
    		rcv = in.read();
    		c = (char) rcv;
    		if ( c == '\n' && d == '\n') done = true;
    		d = c;
    		sb.append(c);
    	}
    	return sb.toString().trim();
	}
	public static void printWelcome() {
		System.out.println("Welcome to Mail Client");
		System.out.println("Connected successfully to Mail Server " + HOST + " on port " + SERVER_PORT);
		printUsage();
	}
	public static void printUsage() {
		final String format = "%-4d %s\n";
		System.out.printf(format,REGISTER,"To Register with the Mail System");
		System.out.printf(format,MESSAGE,"To Send a new Message");
		System.out.printf(format,READ,"To Read your Inbox from the Mail System");
		System.out.printf(format,QUIT,"To Quit this program\n");
	}
	public static int promptForAction() throws Exception {
		boolean done = false;
		boolean isNum = false;
		int choice = REGISTER - 1;
		while (!done || !isNum) {
			System.out.print("Enter the number for the option: ");
			try {
				isNum = true;
				choice = Integer.parseInt(keyboard.readLine());
			} catch (NumberFormatException e) { 
				isNum = false; 
			}
			if (isNum) {
				System.out.println();
				if (choice >= REGISTER && choice <= QUIT)
					done = true;
				else
					printUsage();
			}
		}
		return choice;
	}
	public static void authenticate() throws Exception {
		System.out.print("Please enter your username: ");
		username = keyboard.readLine().trim();
	}
	public static void register() throws Exception {
		boolean done = false;
		String input = null;
		while (!done) {
			System.out.println("Usernames for the Mail System are 15 characters maximum");
			System.out.print("Please enter your desired username: ");
			input = keyboard.readLine().trim();
			System.out.println();
			if (input.length() <= 15) done = true;
		}
		socketSendMessage("USER=NEWUSER\nNAME=" +input+ "\n\n");
		String response = socketReadMessage();
		if (response.equals("USERADDOK")) {
			System.out.println("Successfully registered with the Mail System");
			username = input;
		} else if (response.equals("ERROR=ADD_FAIL")){
			System.out.println("Registering with the Mail System failed");
		} else {
			System.out.println("Transmission error");
		}
	}
	public static void sendMailMessage() throws Exception {
		if (username == null) authenticate();
		System.out.print("Please enter the recipient's username: ");
		String recip = keyboard.readLine().trim();
		System.out.print("Please enter your message and press ENTER when done: ");
		String msg = keyboard.readLine().trim();
		
		socketSendMessage("USER="+username+"\nEXEC=SENDMSG\nTO="+recip+"\nMSG="+msg+"\n\n");
		
		String response = socketReadMessage();
		if (response.equals("MSGSENTOK")) {
			System.out.println("Message sent succesfully");
			System.out.println();
		} else {
			System.out.println("Error sending message");
			System.out.println();
		}
	}
	public static void readInbox() throws Exception {
		if (username == null) authenticate();
		socketSendMessage("USER="+username+"\nEXEC=GETINBOX"+"\n\n");
		String box = socketReadMessage();

		if (box.substring(0,9).equals("INBOXLIST")) {
			printInbox(box);
			System.out.println();
		} else {
			System.out.println("Error getting inbox");
			System.out.println();
		}
	}
	public static void printInbox(String box) {
		ArrayList<String> keyValPairs = Message.decode( box );
		keyValPairs.remove(0);
		int len = keyValPairs.size();
		if (len == 0) {
			System.out.println("Inbox is Empty");
			System.out.println();
		} else {
			String to,from,body;
			System.out.println("Messages in Inbox");
			System.out.printf("%-17s %-17s %-42s\n", "From:", "To:", "Message:");
			for (int i=0; i<len; i+=3) {
				from = Message.extractValue( keyValPairs.get( i ));
				to = Message.extractValue(keyValPairs.get( i+1 ));
				body = Message.extractValue(keyValPairs.get( i+2 ));
				System.out.printf("%-17s %-17s %-42s\n", from, to, body);
			}
		}
	}
}