import static java.lang.System.out;
import java.net.*;
import java.io.*;
public class UnitTester {
	static final int PORT = 65535;
	public static final char DELIM = '\n';
	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("Unit Testing for Mail System: args are test #s to run");
			System.out.println("eg. java UnitTester 0 3 5 9");
			System.out.println("NOTES");
			System.out.println("#1, #2 must be run by two separate instances with #1 starting first");
		} else {
			int i;
			for (String a: args) {
				i = Integer.parseInt(a);
				runTest(i);
			}
		}
	}
	public static void runTest(int i) throws Exception {
		switch (i) {
		case 0: test00(); break;
		case 1: test01(); break;
		case 2: test02(); break;
		case 3: test03(); break;
		case 4: test04(); break;
		case 5: test05(); break;
		}
	}
	public static void test00() {
		out.println("----------TEST00-----------");
		out.println( getMessage00().body );
		out.println("----------TEST00-----------");
	}
	public static void test01() throws Exception {
		out.println("----------TEST01-----------");
		ServerSocket servSock = new ServerSocket(PORT);
		Socket dataSock = servSock.accept();
		PrintWriter sockOut = new PrintWriter( dataSock.getOutputStream() );
		sockOut.print( getMessage00().body + "\n\n");
		sockOut.flush();
		out.println("FLUSHED");
		sockOut.close();
		servSock.close();
		out.println("----------TEST01-----------");
	}
	public static void test02() throws Exception {
		out.println("----------TEST02-----------");
		Socket s = new Socket("localhost", PORT);
		BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		
		String input;
		while ( (input = in.readLine()) != null)
			out.println(input);
		
		in.close();
		out.println("----------TEST02-----------");
	}
	public static void test03() {
		out.println("----------TEST03-----------");
		String s = "KEY=VALUE";
		String t = Message.extractKey(s);
		String u = Message.extractValue(s);
		out.println(t);
		out.println(u);
		out.println("----------TEST03-----------");
	}
	public static void test04() {
		out.println("----------TEST04-----------");
		MailDB db = new MailDB();
		db.addUser("jose");
		db.addUser("sophia");
		db.storeMessage("jose", "sofia", "hello");
		out.println("----------TEST04-----------");
	}
	public static void test05() {
		out.println("----------TEST05-----------");
		out.println( getMessage01() );
		out.println( getMessage02() );
		out.println("----------TEST05-----------");
	}
	
	// TEST DATA
	public static Message getMessage00() {
		String body = "hello"+DELIM+"goodbye"+DELIM+":)";
		return new Message("jose","sofia",body);
	}
	public static Message getMessage01() {
		String body = "A really long message terminated by newline\n";
		return new Message("1234567890123456","012345678",body);
	}
	public static Message getMessage02() {
		String body = "A shorter message\n";
		return new Message("shortname","shorter",body);
	}
}