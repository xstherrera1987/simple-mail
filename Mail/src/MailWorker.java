import static java.util.logging.Level.INFO;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
/**
 *  A MailWorker (Thread) is created by MailServer for every Mail protocol 
 *  message received. The MailWorker interprets the message and executes the
 *  requested operations.  This usually includes responding to the client
 *  over the socket connection.
 */
public class MailWorker extends Thread {
	static final Logger log = Logger.getLogger( MailWorker.class.getName() );
	static final MailDB db = new MailDB();
	static final int PORT = 65535;
	static final int TIMEOUT = 30000;
	
	Socket sock;
	PrintWriter out;
	BufferedReader in;
	ArrayList<String> keyValPairs;
	Map<String, String> data;
	String user, req, msg;
	boolean sessionDone = false;
	public MailWorker(Socket s) {
		super();
		sock = s;
	}
	
	@Override
	public void run() {
		long id = Thread.currentThread().getId();
    	log.log(INFO, "Thread "+id+" started for "+sock.getInetAddress());
		try {
			// open input and output streams with the socket
		    out = new PrintWriter(sock.getOutputStream());
		    in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		    sock.setSoTimeout(TIMEOUT);
		    
		    sessionDone = false;
		    while (!sessionDone) {
		    	// read a message from the socket
		    	StringBuilder sb = new StringBuilder();
		    	int rcv = 0;
		    	char c = (char) 0, d = '\n';
		    	boolean readDone = false;
		    	while ( !readDone && rcv != -1) {
		    		rcv = in.read();
		    		c = (char) rcv;
		    		if ( c == '\n' && d == '\n') readDone = true;
		    		d = c;
		    		sb.append(c);
		    	}
	
			    // tokenize the protocol message into "key=value" tokens
		    	keyValPairs = Message.decode( msg = sb.toString() );
		    	String firstToken = keyValPairs.get(0);
		    	if (firstToken != null && firstToken.equals("END")) {
		    		sessionDone = true;
		    		log.log(INFO, "Thread "+id+" ending session with "+sock.getInetAddress());
		    	} else {
			    	// map each token into a key & value pair
			    	data = Message.getMapping(keyValPairs);
			    	
			    	// serve the request using data stored in instance variables
			    	user = data.get("USER");
					req = data.get("EXEC");
			    	
					if (user != null && user.equals("NEWUSER"))
				    	addNewUser();
				    else if (user != null && req != null) 
				    	serveRequest();
				    else
						log.log(INFO, "Bad message format from "+sock.getInetAddress());
		    	}
		    }
		} catch (Exception e) {
			log.log(INFO, e.toString());
		} finally {
			try {
				out.close();
				sock.close();
//				in.close();
			} catch (Exception e){
				log.log(INFO, e.toString());
			}
		}
	}
	
	public void socketSendMessage(String encoded) {	
		out.print(encoded);
		out.flush();
		log.log(INFO, req +" message sent to "+ user);
	}
	public void addNewUser() {
		String desiredName = data.get("NAME");
		if (desiredName != null) {
			user = desiredName;
			req = "NEWUSER";
			
			if ( db.addUser(user) ) {
				req = "USERADDOK";
				socketSendMessage("USERADDOK\n\n");
			} else {
				req = "ADDFAIL";
				socketSendMessage("ERROR=ADD_FAIL\n\n");
			}
		}
	}
	public void serveRequest() {
		if ( db.authenticateUser(user) ) {
			if ( req.equals("GETINBOX") ) {
				 String box = db.getInbox(user);
				 req = "INBOXLIST";
				 socketSendMessage(box);
			} else if (req.equals("SENDMSG") ) {
				String recip = data.get("TO");
				String body = data.get("MSG");
				if ( db.storeMessage(recip, user, body) )
					socketSendMessage("MSGSENTOK\n\n");
				else 
					socketSendMessage("ERROR=SEND_FAIL\n\n");
			} else if (req.equals("END") ) {
				sessionDone = true;
				log.log(INFO, "Session ending");
			} else {
				log.log(INFO, "Bad message format from "+sock.getInetAddress());
				log.log(INFO, msg);
			}
		} else {
			socketSendMessage("ERROR=AUTH_FAIL\n\n");
		}
	}
}