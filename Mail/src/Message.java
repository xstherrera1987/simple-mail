import java.util.*;
import java.util.logging.*;
/**
 *  A Message is an immutable object that stores the details for a
 *  Mail system Message including the sender, the recipient, and the
 *  message content itself.  This class includes utility methods for
 *  implementors of the Mail protocol.
 */
public class Message {
	static final Logger log = Logger.getLogger( Message.class.getName() );
	public static final char DELIM = '\n';
	public final String from;
	public final String to;
	public final String body;

	public Message(String from, String to, String body) {
		this.from = validUserName(from);
		this.to = validUserName(to);
		this.body = body;
	}
	@Override
	public String toString() {
		return String.format("%-17s %-17s %-42s", from, to, body);
	}
	
	public static Map<String, String> getMapping(ArrayList<String> keyValPairs) {
		HashMap<String,String> hm = new HashMap<String,String>();
		String k,v;
		for (String s: keyValPairs) {
			k = Message.extractKey(s);
			v = Message.extractValue(s);
			hm.put(k, v);
		}
		return hm;
	}
	public static ArrayList<String> decode(String s) {
		StringTokenizer st = new StringTokenizer(s, DELIM+"");
		ArrayList<String> keyValPairs = new ArrayList<String>();
		while (st.hasMoreTokens() )
			keyValPairs.add(st.nextToken());
		return keyValPairs;
	}
	public static String extractValue(String keyValPair) {
		StringBuilder sb = new StringBuilder();
		char[] c = keyValPair.toCharArray();
		int i = 0;
		while (c[i] != '=') i++;
		i += 1;
		for (; i<c.length; i++)
			sb.append(c[i]);
		return sb.toString();
	}
	public static String extractKey(String keyValPair) {
		StringBuilder sb = new StringBuilder();
		char[] c = keyValPair.toCharArray();
		int i = 0;
		while (c[i] != '=') i++;
		for (int j=0; j<i; j++)
			sb.append(c[j]);
		return sb.toString();
	}
	public static String inboxListEncode(Message m) {
		StringBuilder str = new StringBuilder();
		str.append("FROM= " + m.from + DELIM);
		str.append("TO= " + m.to + DELIM);
		str.append("MSG= " + m.body + DELIM);
		return str.toString();
	}
	public static String validUserName(String user) {
		if (user.length() > 15) return user.substring(0,15).toUpperCase();
		else return user.toUpperCase();
	}
}