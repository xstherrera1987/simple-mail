import java.net.*;
import java.util.logging.*;
import static java.util.logging.Level.INFO;
/**
 *  A MailServer serves request for the Mail Protocol Mail System.  It uses
 *  a MailDB database to store messages of the system.  MailServer spawns
 *  a new MailWorker to serve each protocol message received.
 */
public class MailServer {
	static final int PORT = 65532;
	static ServerSocket requestSocket;
	static Socket accepted;
	static Logger log = Logger.getLogger( MailServer.class.getName() );
	public static void main(String[] args) throws Exception {
		requestSocket = new ServerSocket(PORT);
		log.log(INFO, "Server started on port: "+ PORT);
		while (true) {
			// await a connection
			accepted = requestSocket.accept();
			// create a new thread to handle this request 
			new MailWorker(accepted).start();
			log.log(INFO, "Thread dispatched to serve client "+accepted.getInetAddress());
        }
	}
}
