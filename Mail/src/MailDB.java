import java.util.Collection;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.logging.*;
import static java.util.logging.Level.INFO;
/**
 *  MailDB is a simple database for the Mail System. Users can register their
 *  username with the database and MailWorkers can query it for the existence
 *  of those usernames as a primitive authorization.  MailDB stores messages
 *  sent between users in a simple flat file (in memory) that does not persist 
 *  multiple executions of this application.  This database is synchronized for
 *  usage by multiple threads.
 */
public class MailDB {
	static Logger log = Logger.getLogger( MailDB.class.getName() );
	private Collection<Message> messages;
	private Collection<String> users;
	
	public MailDB() {
		users = new TreeSet<String>();
		messages = new ArrayList<Message>();
	}
	public boolean storeMessage(String to, String from, String body) {
		to = Message.validUserName(to);
		from = Message.validUserName(from);
		if ( checkUser(to) ) {
			addMessageToDatabase(new Message(from,to,body));
			log.log(INFO, "USER="+from+" sent message to RCV="+to);
			return true;
		} else {
			log.log(INFO, "USER="+from+" tried to send message to nonexistant RCV="+to);
			return false;
		}
	}
	private synchronized void addMessageToDatabase(Message m) {
		messages.add(m);
	}
	public String getInbox(String user) {
		String u = Message.validUserName(user);
		ArrayList<Message> inbox = new ArrayList<Message>();
		for (Message m: messages)
			if ( m.to.equals(u) ) inbox.add(m);
		
		StringBuilder s = new StringBuilder();
		s.append("INBOXLIST\n");
		for (Message m: inbox)
			s.append( Message.inboxListEncode(m) );
		s.append("\n");
				
		return s.toString();
	}
	public boolean addUser(String user) {
		String u = Message.validUserName(user);
		if ( !checkUser( u ) ) {
			addUserToDatabase(u);
			log.log(INFO, "added USER="+u+" to database");
			return true;
		} else {
			log.log(INFO, "ERROR adding USER="+u+" to database, already exists");
			return false;
		}
	}
	private synchronized void addUserToDatabase(String u) {
		users.add( u );
	}
	private synchronized boolean checkUser(String user) {
		return users.contains( user );
	}
	public boolean authenticateUser(String user) {
		if ( checkUser( Message.validUserName(user) )) {
			log.log(INFO, "Authenticated "+user);
			return true;
		} else {
			log.log(INFO, "Authentication failure for "+user);
			return false;
		}
	}
}